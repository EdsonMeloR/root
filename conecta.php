<?php
    require_once('config.php');
    //Pesquisar: Métodos de conexões com banco de dados mysql (mysqli)
    //Conexão com banco utilizando PDO - Php Data Object
    // $server = 'localhost';    
    $user = 'root';
    $banco = 'aulaphpdb';
    $password = 'usbw';

    try
    {
        $conn = new PDO('mysql:host = '.SERVIDOR_DB.';dbname = '.$banco,$user,$password);
        $conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        echo "Sucesso na conexão <br>";
    }
    catch(PDOException $ex)
    {
        echo 'Erro : '.$ex->getMessage();
    }    
?>