-- Criando banco de dados
create database aulaphpdb;
-- Definindo banco ativo
use aulaphpdb;
-- Criando tabelas

create table aluno
(
    id int not null primary key auto_increment,
    nome varchar(60) not null,
    cpf varchar(11) unique not null,
    email varchar(60) not null unique,
    data_cad TIMESTAMP DEFAULT current_timestamp
);