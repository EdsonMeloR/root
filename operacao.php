
<?php
    require_once('funcoes.php');
    require_once('config.php');
    #$montante = calcular_montante(3000,1.73,36);
    if(isset($_GET['txt_taxa']) && isset($_GET['btn_calcula_taxa']))
    {
        $capital = $_GET['txt_capital'];        
        $parcelas = $_GET['txt_parcelas'];
        $montante = $_GET['txt_montante'];
        $taxa = calcular_taxa($montante,$capital,$parcelas);

        //var_dump($capital);
        echo "<br>";
        //var_dump($taxa);
        echo "<br>";
        //var_dump($parcelas);
        echo "<br>";
        //var_dump($montante);
    }
    else if(isset($_GET['txt_montante']) && isset($_GET['btn_calcula_montante']))
    {
        $capital = $_GET['txt_capital'];
        $taxa = $_GET['txt_taxa'];
        $parcelas = $_GET['txt_parcelas'];
        $montante = calcular_montante($capital,$taxa,$parcelas);
        
    }    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        h1
        {
            width: 100%;
            text-align: center;
        }
        tr
        {
            text-align: center;
        }
        td
        {
            text-align: center;
        }
    </style>
</head>
<body>
<h1>Fazendo calculo de Juro Composto</h1>
<form action="#" method="get">
    <fieldset>
        <label for="" >Capital R$</label>
        <input type="text" name="txt_capital" value="<?php echo isset($capital)? number_format($capital,2,',','.'):"00,0" ?>" required>
        <br>
        <label for="">Taxa 
            <input type="text" name="txt_taxa" value=<?php echo isset($taxa)? $taxa.' %':"00,0" ?> required>    (% a.m.)
        </label>
        
        <br>
        <label for="">Numero de parcelas 
            <input type="text" name="txt_parcelas" value=<?php echo isset($parcelas)? $parcelas: 0; ?> required> (Meses)        
        </label>        
        <br>
        <label for="">Montate</label>
        <input type="text" name="txt_montante" value=<?php echo isset($montante)? number_format($montante,2,',','.'): "0"; ?> required>
        <br><br>
        <input type="submit" name="btn_calcula_montante" value="Calcular Montante" required>
        <input type="submit" name="btn_calcula_taxa" value="Calcular Taxa" required>
    </fieldset>
</form>
<h2>Valor total do financiamento</h2>
    <?php //var_dump($montante);?>
<h3>Total R$: <?php echo isset($montante)? number_format($montante,2,',','.') : "0,00" ; ?></h3>
    <?php //var_dump($montante);?>
<h4>
    <?php
        if(isset($montante))
        {               
            //var_dump($montante);
            $valor_parcelas = number_format($montante/$parcelas,2,',','.'); 
            //var_dump($valor_parcelas);
            echo $parcelas. ' Parcelas de R$ '.$valor_parcelas;
            echo "<br>";
            echo "<br>";
            echo "<table border=1>";
            echo "<tr><td>N° parcela</td><td>Valor Parcela</td><td>Dia do pagamento</td><td>Taxa</td></tr>";            
            for($i = 1; $i <= $parcelas; $i++)
            {
                $data = new DateTime();                 
                $data = new DateTime('+'. 30*$i.'days');
                echo "<tr><td>".$i." </td><td>".$valor_parcelas." R$</td><td>".$data->format('d-m-Y')."</td><td>". number_format($taxa,3)." % </td></tr>";                
            }
            echo "</table>";

        }
        else
        {
            echo money_format('%i', $montante);
        }
    ?>    
</h4>
</body>
</html>

