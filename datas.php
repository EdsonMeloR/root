<?php
require_once('config.php');
//Exibindo data atual
    echo date('d/m/Y');
    echo "<br>";
    echo date ('H:i:s');
    echo "<br>";
//Exibindo data, hora, minuto, segundo atual.    
    echo date('Y-m-d H:i:s e ');
    echo "<br>";
    $timestamp = time();
    echo $timestamp;
    echo "<br>";
//Manipulando datas, levando em consideração
//Que o PHP conta data apartir de 01/01/1970 01:00:00
    echo date('d-m-Y h:i:s','4');
    echo "<br>";
    echo phpversion();
//Manipulando mktime, função para criar uma data
    $timest = mktime(15,21,05,07,20,2018);
    echo "<br>";
    echo $timest;
    echo "<br>";
//Utilizando o parametro W (maiusculo) mostra a semana que do ano
    echo date('W',$timest);
    echo "<br>";
//O parametro w (minusculo) representa o dia da semana começando a partir do 0 = Domingo
    echo date('w',$timest);
    echo "<br>";
//O parametro l(Minusculo) representa o dia completo da semana de forma textual
    echo "<hr>";
    echo date('l',$timest);
    echo "<br>";
//O parametro F(Maiusculo) representa o mês da data de forma textual
    echo "<hr>";
    echo date('F',$timest);
    echo "<br>";
//O paramentro C representa a data e hora completa
    echo "<hr>";
    echo date('c',$timest);
    echo "<br>";
//O parametro P(maiusculo) mostra a data com o fuso horario separado por : dois pontos 
    echo "<hr>";
    echo date('P',$timest);
    echo "<br>";
//O paramentro r(minusculo) mostra a data completa com todos espaços
    echo "<hr>";
    echo date('r',$timest);
    echo "<br>";        
    $date = '1998-10-09 18:25:30';
    $timestamp1 = strtotime($date);
    $timestamp2 = strtotime('+1 day', $timestamp1);
// Resultado em timestamp
    echo "<hr>";
    echo $timestamp1."<br>";
    echo $timestamp2."<br>";
// Resultado em data;
    echo "<hr>";
    echo date('d-m-Y h-i-s',$timestamp1);
    echo "<br>";
    echo date('d-m-Y h-i-s',$timestamp2);
    echo "<br>";

    $tz = new DateTimeZone("America/Sao_Paulo");//var timezone = new DateTimeZone("America/Sao_Paulo");
    print_r($tz->getLocation());//timezone.getLocation(); 
    echo "<br>";
    print_r(timezone_location_get($tz));
    echo "<br>";
    echo "<hr>";
    //Usando datatimezone america são paulo
    $date = date_create('1998-06-05', timezone_open('America/Sao_Paulo'));
    echo date_format($date, 'd-m-Y H:i:s P') . "\n";
    echo "<br>";
    echo "<hr>";
    $data_padrao = new DateTime();
    print_r($data_padrao);
    echo "<br>";

    $data_financiamento = new DateTime('17-09-2019 15:32:00');
    $dia_amanha = new DateTime('+1 day');

    print_r($data_financiamento);
    echo "<br>";
    print_r($dia_amanha);
    echo "<br>";
    echo $data_financiamento->format('d m Y');
    echo "<br>";    
?>